//
// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Software-only trait implementations.

use alloc::boxed::Box;
use alloc::vec::Vec;
use kmr_common::{crypto, Error};
use kmr_ta::device::DeviceHmac;
use kmr_ta::device::RetrieveKeyMaterial;
use kmr_wire::keymint::Digest;

const ROOT_KEK_SIZE: usize = 16;
const ROOT_KEK_DERIVATION_DATA: &'static [u8] = b"KeymasterMaster\0";

const KAK_SIZE: usize = 32;
const KAK_DERIVATION_DATA: &'static [u8] = b"KeymintKeyAgreementKey\0";

const AUTHTOKEY_KEY_SIZE: usize = 32;
const AUTHTOKEN_KEY_DERIVATION_DATA: &'static [u8] = b"KeymintAuthTokenKey\0";

pub struct Keys;

impl RetrieveKeyMaterial for Keys {
    fn root_kek(&self, _context: &[u8]) -> Result<crypto::OpaqueOr<crypto::hmac::Key>, Error> {
        let key = crate::crypto::derive_hbk(ROOT_KEK_DERIVATION_DATA, ROOT_KEK_SIZE)?;
        Ok(crypto::hmac::Key::new(key.0.clone()).into())
    }

    fn kak(&self) -> Result<crypto::OpaqueOr<crypto::aes::Key>, Error> {
        // In a real implementation with multiple TEEs (e.g. Strongbox), KAK
        // is provisioned to the device. With only a single TEE, deriving a
        // hardware-backed key is sufficient.
        let key = crate::crypto::derive_hbk(KAK_DERIVATION_DATA, KAK_SIZE)?;
        Ok(crypto::aes::Key::Aes256(key.0.clone().try_into().unwrap()).into())
    }

    fn hmac_key_agreed(&self, _key: &crypto::hmac::Key) -> Option<Box<dyn DeviceHmac>> {
        if let Ok(hmac) = AuthTokenHmac::try_new() {
            Some(Box::new(hmac))
        } else {
            None
        }
    }
}

// Hardware auth token HMAC key derived from hardware unique key.
pub struct AuthTokenHmac {
    key: crypto::hmac::Key,
}

impl AuthTokenHmac {
    pub fn try_new() -> Result<Self, Error> {
        let raw_key = crate::crypto::derive_hbk(AUTHTOKEN_KEY_DERIVATION_DATA, AUTHTOKEY_KEY_SIZE)?;
        let key = crypto::hmac::Key::new(raw_key.0.clone()).into();
        Ok(AuthTokenHmac { key })
    }
}

impl DeviceHmac for AuthTokenHmac {
    fn hmac(&self, imp: &dyn crypto::Hmac, data: &[u8]) -> Result<Vec<u8>, Error> {
        let mut hmac_op = imp.begin(self.key.clone().into(), Digest::Sha256)?;
        hmac_op.update(data)?;
        hmac_op.finish()
    }

    fn get_hmac_key(&self) -> Option<crypto::hmac::Key> {
        Some(self.key.clone())
    }
}
