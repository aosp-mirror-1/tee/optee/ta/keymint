//
// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::invoke::{Direction, Param, Ta, PTA_SYSTEM_DERIVE_TA_UNIQUE_KEY};
use alloc::vec::Vec;
use kmr_common::crypto::RawKeyMaterial;
use kmr_common::vec_try;
use kmr_wire::keymint::Digest;
use optee_utee::AlgorithmId;

pub mod aes;
pub mod clock;
pub mod des;
pub mod ec;
pub mod eq;
pub mod mac;
pub mod operation;
pub mod rng;
pub mod rsa;
pub mod sha;

#[cfg(feature = "dev")]
pub mod tests;

pub trait FfiSlice<T> {
    // Returns a valid pointer also for an empty Vec:
    // https://github.com/rust-lang/rust/issues/39625
    fn as_ffi_slice(&self) -> &[T];
}

impl<T> FfiSlice<T> for Vec<T> {
    fn as_ffi_slice(&self) -> &[T] {
        if self.len() > 0 {
            self.as_slice()
        } else {
            &[]
        }
    }
}

impl<T> FfiSlice<T> for &[T] {
    fn as_ffi_slice(&self) -> &[T] {
        if self.len() > 0 {
            self
        } else {
            &[]
        }
    }
}

fn digest_to_algid(digest: Digest) -> Option<AlgorithmId> {
    match digest {
        Digest::None => None,
        Digest::Md5 => Some(AlgorithmId::Md5),
        Digest::Sha1 => Some(AlgorithmId::Sha1),
        Digest::Sha224 => Some(AlgorithmId::Sha224),
        Digest::Sha256 => Some(AlgorithmId::Sha256),
        Digest::Sha384 => Some(AlgorithmId::Sha384),
        Digest::Sha512 => Some(AlgorithmId::Sha512),
    }
}

pub fn derive_hbk(input: &[u8], len: usize) -> Result<RawKeyMaterial, kmr_common::Error> {
    let mut params = Param::empty();
    let session = Ta::open_system_pta(&params)?;

    let mut key = vec_try![0u8; len]?;

    params.0 = Param::from_slice(input);
    params.1 = Param::from_mut_slice(key.as_mut_slice(), Direction::Out);
    session.invoke(PTA_SYSTEM_DERIVE_TA_UNIQUE_KEY, &params)?;

    Ok(RawKeyMaterial(key))
}
