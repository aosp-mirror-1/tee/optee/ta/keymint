//
// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::crypto::FfiSlice;
use alloc::vec::Vec;
use der::asn1::BitStringRef;
use der::referenced::RefToOwned;
use hex;
use kmr_common::crypto::{
    aes, des, ec, hmac, rsa, Aes, AesCmac, ConstTimeEq, CurveType, Des, Ec, Hmac, KeyGenInfo,
    KeyMaterial, MonotonicClock, OpaqueOr, Rng, Rsa, Sha256, SymmetricOperation,
};
use kmr_wire::keymint::Digest;
use log::{error, info};
use pkcs8;
use pkcs8::spki::{AlgorithmIdentifier, SubjectPublicKeyInfoRef};

/// Test basic hardware-backed key derivation functionality.
fn test_derive_hbk() {
    let key1 = crate::crypto::derive_hbk("Secret".as_bytes(), 16).unwrap();
    let key2 = crate::crypto::derive_hbk("Secret".as_bytes(), 16).unwrap();
    let key3 = crate::crypto::derive_hbk("Sekret".as_bytes(), 16).unwrap();
    assert_eq!(key1.0, key2.0);
    assert_ne!(key1.0, key3.0);
    info!("test_derive_hbk: Success");
}

/// Test basic [`MonotonicClock`] functionality.
pub fn test_clock<C: MonotonicClock>(clock: C) {
    let t1 = clock.now();
    assert!(t1.0 > 0);
    let t2 = clock.now();
    assert!(t2.0 >= t1.0);
    let t3 = clock.now();
    assert!(t3.0 >= t2.0);
    info!("test_clock: Success");
}

/// Test basic [`Rng`] functionality.
pub fn test_rng<R: Rng>(rng: &mut R) {
    let u1 = rng.next_u64();
    let u2 = rng.next_u64();
    assert_ne!(u1, u2);
    let mut b1 = [0u8; 16];
    let mut b2 = [0u8; 16];
    rng.fill_bytes(&mut b1);
    rng.fill_bytes(&mut b2);
    assert_ne!(b1, b2);
    rng.add_entropy(&b1);
    rng.add_entropy(&[]);
    rng.fill_bytes(&mut b1);
    assert_ne!(b1, b2);
    info!("test_rng: Success");
}

/// Test basic [`ConstTimeEq`] functionality. Does not test the key constant-time property though.
pub fn test_eq<E: ConstTimeEq>(comparator: E) {
    let b0 = [];
    let b1 = [0u8, 1u8, 2u8];
    let b2 = [1u8, 1u8, 2u8];
    let b3 = [0u8, 1u8, 3u8];
    let b4 = [0u8, 1u8, 2u8, 3u8];
    let b5 = [42; 4096];
    let mut b6 = [42; 4096];
    b6[4095] = 43;
    assert!(comparator.eq(&b0, &b0));
    assert!(comparator.eq(&b5, &b5));
    assert!(comparator.ne(&b0, &b1));
    assert!(comparator.ne(&b0, &b2));
    assert!(comparator.ne(&b0, &b3));
    assert!(comparator.ne(&b0, &b4));
    assert!(comparator.ne(&b0, &b5));
    assert!(comparator.eq(&b1, &b1));
    assert!(comparator.ne(&b1, &b2));
    assert!(comparator.ne(&b1, &b3));
    assert!(comparator.ne(&b5, &b4));
    assert!(comparator.ne(&b5, &b6));
    info!("test_eq: Success");
}

/// Test basic SHA-256 functionality.
pub fn test_sha256<S: Sha256>(sha256: S) {
    struct TestCase {
        msg: &'static [u8],
        want: &'static str,
    }
    let tests = vec![
        TestCase {
            msg: b"",
            want: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
        },
        TestCase {
            msg: b"abc",
            want: "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad",
        },
    ];
    for test in tests {
        let got = sha256.hash(test.msg).unwrap();
        assert_eq!(hex::encode(&got), test.want, "for input {}", hex::encode(test.msg));
    }
    info!("test_sha256: Success");
}

/// Test basic [`Hmac`] functionality.
pub fn test_hmac<H: Hmac>(hmac: H) {
    struct TestCase {
        digest: Digest,
        tag_size: usize,
        key: &'static [u8],
        data: &'static [u8],
        expected_mac: &'static str,
    }
    const HMAC_TESTS: &[TestCase] = &[
        TestCase {
            digest: Digest::Sha256,
            tag_size:     32,
            data:         b"Hello",
            key:          b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f",
            expected_mac: "e0ff02553d9a619661026c7aa1ddf59b7b44eac06a9908ff9e19961d481935d4",
        },
        TestCase {
            digest: Digest::Sha512,
            tag_size:     64,
            data:         b"Hello",
            key:          b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f",
            expected_mac: "481e10d823ba64c15b94537a3de3f253c16642451ac45124dd4dde120bf1e5c15e55487d55ba72b43039f235226e7954cd5854b30abc4b5b53171a4177047c9b",
        },
        // empty data
        TestCase {
            digest: Digest::Sha256,
            tag_size:     32,
            data:         &[],
            key:          b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f",
            expected_mac: "07eff8b326b7798c9ccfcbdbe579489ac785a7995a04618b1a2813c26744777d",
        },
        // Test cases from RFC 4231 Section 4.2
        TestCase {
            digest: Digest::Sha224,
            tag_size: 224/8,
            key: &[0x0b; 20],
            data: b"Hi There",
            expected_mac: concat!(
                "896fb1128abbdf196832107cd49df33f",
                "47b4b1169912ba4f53684b22",
            ),
        },
        TestCase {
            digest: Digest::Sha256,
            tag_size: 256/8,
            key: &[0x0b; 20],
            data: b"Hi There",
            expected_mac: concat!(
                "b0344c61d8db38535ca8afceaf0bf12b",
                "881dc200c9833da726e9376c2e32cff7",
            ),
        },
        TestCase {
            digest: Digest::Sha384,
            tag_size: 384/8,
            key: &[0x0b; 20],
            data: b"Hi There",
            expected_mac: concat!(
                "afd03944d84895626b0825f4ab46907f",
                "15f9dadbe4101ec682aa034c7cebc59c",
                "faea9ea9076ede7f4af152e8b2fa9cb6",
            ),
        },
        TestCase {
            digest: Digest::Sha512,
            tag_size: 512/8,
            key: &[0x0b; 20],
            data: b"Hi There",
            expected_mac: concat!(
                "87aa7cdea5ef619d4ff0b4241a1d6cb0",
                "2379f4e2ce4ec2787ad0b30545e17cde",
                "daa833b7d6b8a702038b274eaea3f4e4",
                "be9d914eeb61f1702e696c203a126854"
            ),
        },
        // Test cases from RFC 4231 Section 4.3 dropped because OP-TEE enforces
        // a 64-bit minimum key length
        // Test cases from RFC 4231 Section 4.4
        TestCase {
            digest: Digest::Sha224,
            tag_size: 224/8,
            key: &[0xaa; 20],
            data: &[0xdd; 50],
            expected_mac: concat!(
                "7fb3cb3588c6c1f6ffa9694d7d6ad264",
                "9365b0c1f65d69d1ec8333ea"
            ),
        },
        TestCase {
            digest: Digest::Sha256,
            tag_size: 256/8,
            key: &[0xaa; 20],
            data: &[0xdd; 50],
            expected_mac: concat!(
                "773ea91e36800e46854db8ebd09181a7",
                "2959098b3ef8c122d9635514ced565fe"
            ),
        },
        TestCase {
            digest: Digest::Sha384,
            tag_size: 384/8,
            key: &[0xaa; 20],
            data: &[0xdd; 50],
            expected_mac: concat!(
                "88062608d3e6ad8a0aa2ace014c8a86f",
                "0aa635d947ac9febe83ef4e55966144b",
                "2a5ab39dc13814b94e3ab6e101a34f27"
            ),
        },
        TestCase {
            digest: Digest::Sha512,
            tag_size: 512/8,
            key: &[0xaa; 20],
            data: &[0xdd; 50],
            expected_mac: concat!(
                "fa73b0089d56a284efb0f0756c890be9",
                "b1b5dbdd8ee81a3655f83e33b2279d39",
                "bf3e848279a722c806b485a47e67c807",
                "b946a337bee8942674278859e13292fb"
            ),
        },
    ];
    for (i, test) in HMAC_TESTS.iter().enumerate() {
        let mut op = hmac.begin(hmac::Key(test.key.to_vec()).into(), test.digest).unwrap();
        op.update(test.data).unwrap();
        let mut mac = op.finish().unwrap();
        mac.truncate(test.tag_size);
        assert_eq!(
            hex::encode(&mac),
            test.expected_mac[..(test.tag_size * 2)],
            "incorrect mac in test case {}",
            i
        );
    }
    info!("test_hmac: Success");
}

/// Test [`AesCmac`] functionality with a 128-bit key.
pub fn test_aes_cmac_128<M: AesCmac>(cmac: M) {
    // Test vectors from RFC 4493.
    let key = hex::decode("2b7e151628aed2a6abf7158809cf4f3c").unwrap();
    let key = aes::Key::new(key).unwrap();
    let data = hex::decode("6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710").unwrap();
    let expected = vec![
        (0usize, "bb1d6929e95937287fa37d129b756746"),
        (16usize, "070a16b46b4d4144f79bdd9dd04a287c"),
        (40usize, "dfa66747de9ae63030ca32611497c827"),
        (64usize, "51f0bebf7e3b9d92fc49741779363cfe"),
    ];
    for (len, want) in expected {
        let mut op = cmac.begin(key.clone().into()).unwrap();
        op.update(&data[..len]).unwrap();
        let cmac = op.finish().unwrap();
        assert_eq!(hex::encode(&cmac[..16]), want);
    }

    info!("test_aes_cmac_128: Success");
}

/// Test [`AesCmac`] functionality with a 192-bit key.
pub fn test_aes_cmac_192<M: AesCmac>(cmac: M) {
    // Test vectors from SP 800-38B.
    let key = hex::decode("8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b").unwrap();
    let key = aes::Key::new(key).unwrap();
    let data = hex::decode("6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710").unwrap();
    let expected = vec![
        (0usize, "d17ddf46adaacde531cac483de7a9367"),
        (16usize, "9e99a7bf31e710900662f65e617c5184"),
        (40usize, "8a1de5be2eb31aad089a82e6ee908b0e"),
        (64usize, "a1d5df0eed790f794d77589659f39a11"),
    ];
    for (len, want) in expected {
        let mut op = cmac.begin(key.clone().into()).unwrap();
        op.update(&data[..len]).unwrap();
        let cmac = op.finish().unwrap();
        assert_eq!(hex::encode(&cmac[..16]), want);
    }

    info!("test_aes_cmac_192: Success");
}

/// Test [`AesCmac`] functionality with a 256-bit key.
pub fn test_aes_cmac_256<M: AesCmac>(cmac: M) {
    // Test vectors from SP 800-38B.
    let key =
        hex::decode("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4").unwrap();
    let key = aes::Key::new(key).unwrap();
    let data = hex::decode("6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710").unwrap();
    let expected = vec![
        (0usize, "028962f61b7bf89efc6b551f4667d983"),
        (16usize, "28a7023f452e8f82bd4bf28d8c37c35c"),
        (40usize, "aaf3d8f1de5640c232f5b169b9c911e6"),
        (64usize, "e1992190549f6ed5696a2c056c315410"),
    ];
    for (len, want) in expected {
        let mut op = cmac.begin(key.clone().into()).unwrap();
        op.update(&data[..len]).unwrap();
        let cmac = op.finish().unwrap();
        assert_eq!(hex::encode(&cmac[..16]), want);
    }

    info!("test_aes_cmac_256: Success");
}

/// Test basic [`Aes`] functionality.
pub fn test_aes<A: Aes>(aes: A) {
    struct TestCase {
        input: &'static str,
        output: &'static str,
    }

    // AES-128 CTR
    // https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38a.pdf F.5.1
    let key = aes::Key::new(hex::decode("2b7e151628aed2a6abf7158809cf4f3c").unwrap()).unwrap();
    let iv = hex::decode("f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff").unwrap();

    const CTR128_TESTS: &[TestCase] = &[
        TestCase {
            input: "6bc1bee22e409f96e93d7e117393172a",
            output: "874d6191b620e3261bef6864990db6ce",
        },
        TestCase {
            input: "ae2d8a571e03ac9c9eb76fac45af8e51",
            output: "9806f66b7970fdff8617187bb9fffdff",
        },
        TestCase {
            input: "30c81c46a35ce411e5fbc1191a0a52ef",
            output: "5ae4df3edbd5d35e5b4f09020db03eab",
        },
        TestCase {
            input: "f69f2445df4f9b17ad2b417be66c3710",
            output: "1e031dda2fbe03d1792170a0f3009cee",
        },
    ];

    let mut encrypt = aes
        .begin(
            key.clone().into(),
            aes::CipherMode::Ctr { nonce: iv.as_slice().try_into().unwrap() },
            SymmetricOperation::Encrypt,
        )
        .unwrap();
    let mut decrypt = aes
        .begin(
            key.clone().into(),
            aes::CipherMode::Ctr { nonce: iv.as_slice().try_into().unwrap() },
            SymmetricOperation::Decrypt,
        )
        .unwrap();

    for test in CTR128_TESTS.iter() {
        let input: [u8; 16] = hex::decode(test.input).unwrap().as_slice().try_into().unwrap();
        let mut output: [u8; 16] = encrypt.update(&input).unwrap().try_into().unwrap();
        assert_eq!(output, hex::decode(test.output).unwrap().as_slice());
        output = decrypt.update(&output).unwrap().try_into().unwrap();
        assert_eq!(input, output);
    }

    // AES-256 CTR
    // https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38a.pdf F.5.5
    let key = aes::Key::new(
        hex::decode("603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4").unwrap(),
    )
    .unwrap();
    let iv = hex::decode("f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff").unwrap();

    const CTR256_TESTS: &[TestCase] = &[
        TestCase {
            input: "6bc1bee22e409f96e93d7e117393172a",
            output: "601ec313775789a5b7a7f504bbf3d228",
        },
        TestCase {
            input: "ae2d8a571e03ac9c9eb76fac45af8e51",
            output: "f443e3ca4d62b59aca84e990cacaf5c5",
        },
        TestCase {
            input: "30c81c46a35ce411e5fbc1191a0a52ef",
            output: "2b0930daa23de94ce87017ba2d84988d",
        },
        TestCase {
            input: "f69f2445df4f9b17ad2b417be66c3710",
            output: "dfc9c58db67aada613c2dd08457941a6",
        },
    ];

    let mut encrypt = aes
        .begin(
            key.clone().into(),
            aes::CipherMode::Ctr { nonce: iv.as_slice().try_into().unwrap() },
            SymmetricOperation::Encrypt,
        )
        .unwrap();
    let mut decrypt = aes
        .begin(
            key.clone().into(),
            aes::CipherMode::Ctr { nonce: iv.as_slice().try_into().unwrap() },
            SymmetricOperation::Decrypt,
        )
        .unwrap();

    for test in CTR256_TESTS.iter() {
        let input: [u8; 16] = hex::decode(test.input).unwrap().as_slice().try_into().unwrap();
        let mut output: [u8; 16] = encrypt.update(&input).unwrap().try_into().unwrap();
        assert_eq!(output, hex::decode(test.output).unwrap().as_slice());
        output = decrypt.update(&output).unwrap().try_into().unwrap();
        assert_eq!(input, output);
    }

    // AES-CBC w/ PKCS#7 padding
    struct CBCTestCase {
        key: &'static str,
        iv: &'static str,
        input: &'static str,
        output: &'static str,
    }

    const CBC_TESTS: &[CBCTestCase] = &[
        // https://github.com/google/wycheproof/blob/master/testvectors/aes_cbc_pkcs5_test.json
        // "tcId" : 1
        CBCTestCase {
            key: "e34f15c7bd819930fe9d66e0c166e61c",
            iv: "da9520f7d3520277035173299388bee2",
            input: "",
            output: "b10ab60153276941361000414aed0a9d",
        },
        // "tcId" : 2
        CBCTestCase {
            key: "e09eaa5a3f5e56d279d5e7a03373f6ea",
            iv: "c9ee3cd746bf208c65ca9e72a266d54f",
            input: "ef4eab37181f98423e53e947e7050fd0",
            output: "d1fa697f3e2e04d64f1a0da203813ca5bc226a0b1d42287b2a5b994a66eaf14a",
        },
        // "tcId" : 20
        CBCTestCase {
            key: "831e664c9e3f0c3094c0b27b9d908eb2",
            iv: "54f2459e40e002763144f4752cde2fb5",
            input: "26603bb76dd0a0180791c4ed4d3b058807",
            output: "8d55dc10584e243f55d2bdbb5758b7fabcd58c8d3785f01c7e3640b2a1dadcd9",
        },
        // "tcId" : 124
        CBCTestCase {
            key: "612e837843ceae7f61d49625faa7e7494f9253e20cb3adcea686512b043936cd",
            iv: "9ec7b863ac845cad5e4673da21f5b6a9",
            input: "cc37fae15f745a2f40e2c8b192f2b38d",
            output: "299295be47e9f5441fe83a7a811c4aeb2650333e681e69fa6b767d28a6ccf282",
        },
        // "tcId" : 128
        CBCTestCase {
            key: "ea3b016bdd387dd64d837c71683808f335dbdc53598a4ea8c5f952473fafaf5f",
            iv: "fae3e2054113f6b3b904aadbfe59655c",
            input: "6601",
            output: "b90c326b72eb222ddb4dae47f2bc223c",
        },
        // "tcId" : 141
        CBCTestCase {
            key: "73216fafd0022d0d6ee27198b2272578fa8f04dd9f44467fbb6437aa45641bf7",
            iv: "4b74bd981ea9d074757c3e2ef515e5fb",
            input: "d5247b8f6c3edcbfb1d591d13ece23d2f5",
            output: "fbea776fb1653635f88e2937ed2450ba4e9063e96d7cdba04928f01cb85492fe",
        },
    ];

    for test in CBC_TESTS {
        let key = aes::Key::new(hex::decode(test.key).unwrap()).unwrap();
        let iv = hex::decode(test.iv).unwrap();

        let mut encrypt = aes
            .begin(
                key.clone().into(),
                aes::CipherMode::CbcPkcs7Padding { nonce: iv.as_slice().try_into().unwrap() },
                SymmetricOperation::Encrypt,
            )
            .unwrap();
        let mut encrypt2 = aes
            .begin(
                key.clone().into(),
                aes::CipherMode::CbcPkcs7Padding { nonce: iv.as_slice().try_into().unwrap() },
                SymmetricOperation::Encrypt,
            )
            .unwrap();
        let mut decrypt = aes
            .begin(
                key.clone().into(),
                aes::CipherMode::CbcPkcs7Padding { nonce: iv.as_slice().try_into().unwrap() },
                SymmetricOperation::Decrypt,
            )
            .unwrap();
        let mut decrypt2 = aes
            .begin(
                key.clone().into(),
                aes::CipherMode::CbcPkcs7Padding { nonce: iv.as_slice().try_into().unwrap() },
                SymmetricOperation::Decrypt,
            )
            .unwrap();

        let input = hex::decode(test.input).unwrap();
        let mut encrypted = encrypt.update(input.as_slice()).unwrap();
        encrypted.extend_from_slice(encrypt.finish().unwrap().as_slice());
        assert_eq!(encrypted, hex::decode(test.output).unwrap());

        let mut encrypted_by_byte = Vec::<u8>::new();
        for b in input {
            encrypted_by_byte
                .extend_from_slice(encrypt2.update(core::slice::from_ref(&b)).unwrap().as_slice());
        }
        encrypted_by_byte.extend_from_slice(encrypt2.finish().unwrap().as_slice());
        assert_eq!(encrypted_by_byte, hex::decode(test.output).unwrap());

        let mut decrypted = decrypt.update(encrypted.as_slice()).unwrap();
        decrypted.extend_from_slice(decrypt.finish().unwrap().as_slice());
        assert_eq!(decrypted, hex::decode(test.input).unwrap());

        let mut decrypted_by_byte = Vec::<u8>::new();
        for b in encrypted_by_byte {
            decrypted_by_byte
                .extend_from_slice(decrypt2.update(core::slice::from_ref(&b)).unwrap().as_slice());
        }
        decrypted_by_byte.extend_from_slice(decrypt2.finish().unwrap().as_slice());
        assert_eq!(decrypted_by_byte, hex::decode(test.input).unwrap());
    }

    info!("test_aes: Success");
}

/// Test basic [`Aes`] functionality with AES-GCM.
pub fn test_aes_gcm<A: Aes>(aes: A) {
    struct TestCase {
        key: &'static str,
        iv: &'static str,
        aad: &'static str,
        msg: &'static str,
        ct: &'static str,
        tag: &'static str,
    }
    // Test vectors from https://github.com/google/wycheproof/blob/master/testvectors/aes_gcm_test.json
    const AES_GCM_TESTS: &[TestCase] = &[
        TestCase {
            key: "5b9604fe14eadba931b0ccf34843dab9",
            iv: "028318abc1824029138141a2",
            aad: "",
            msg: "001d0c231287c1182784554ca3a21908",
            ct: "26073cc1d851beff176384dc9896d5ff",
            tag: "0a3ea7a5487cb5f7d70fb6c58d038554",
        },
        TestCase {
            key: "5b9604fe14eadba931b0ccf34843dab9",
            iv: "921d2507fa8007b7bd067d34",
            aad: "00112233445566778899aabbccddeeff",
            msg: "001d0c231287c1182784554ca3a21908",
            ct: "49d8b9783e911913d87094d1f63cc765",
            tag: "1e348ba07cca2cf04c618cb4d43a5b92",
        },
    ];
    for test in AES_GCM_TESTS {
        let key = hex::decode(test.key).unwrap();
        let iv = hex::decode(test.iv).unwrap();
        assert_eq!(iv.len(), 12); // Only 96-bit nonces supported.
        let aad = hex::decode(test.aad).unwrap();
        let msg = hex::decode(test.msg).unwrap();
        let tag = hex::decode(test.tag).unwrap();
        assert_eq!(tag.len(), 16); // Test data includes full 128-bit tag

        let aes_key = aes::Key::new(key.clone()).unwrap();
        let mut op = aes
            .begin_aead(
                aes_key.into(),
                aes::GcmMode::GcmTag16 { nonce: iv.clone().try_into().unwrap() },
                SymmetricOperation::Encrypt,
            )
            .unwrap();
        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut got_ct = op.update(&msg).unwrap();
        got_ct.extend_from_slice(&op.finish().unwrap());
        assert_eq!(format!("{}{}", test.ct, test.tag), hex::encode(&got_ct));
        let aes_key = aes::Key::new(key.clone()).unwrap();
        let mut op = aes
            .begin_aead(
                aes_key.into(),
                aes::GcmMode::GcmTag16 { nonce: iv.clone().try_into().unwrap() },
                SymmetricOperation::Decrypt,
            )
            .unwrap();
        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut got_pt = op.update(&got_ct).unwrap();
        got_pt.extend_from_slice(&op.finish().unwrap());
        assert_eq!(test.msg, hex::encode(&got_pt));

        // One byte at a time
        let aes_key = aes::Key::new(key.clone()).unwrap();
        let mut op = aes
            .begin_aead(
                aes_key.into(),
                aes::GcmMode::GcmTag16 { nonce: iv.clone().try_into().unwrap() },
                SymmetricOperation::Encrypt,
            )
            .unwrap();

        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut got_ct = Vec::<u8>::new();
        for b in &msg {
            got_ct.extend_from_slice(op.update(core::slice::from_ref(&b)).unwrap().as_slice());
        }
        got_ct.extend_from_slice(op.finish().unwrap().as_slice());
        assert_eq!(format!("{}{}", test.ct, test.tag), hex::encode(&got_ct));
        let aes_key = aes::Key::new(key.clone()).unwrap();
        let mut op = aes
            .begin_aead(
                aes_key.into(),
                aes::GcmMode::GcmTag16 { nonce: iv.clone().try_into().unwrap() },
                SymmetricOperation::Decrypt,
            )
            .unwrap();
        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut got_pt = Vec::<u8>::new();
        for b in &got_ct {
            got_pt.extend_from_slice(op.update(core::slice::from_ref(&b)).unwrap().as_slice());
        }
        got_pt.extend_from_slice(op.finish().unwrap().as_slice());
        assert_eq!(test.msg, hex::encode(&got_pt));

        // Truncated tag should still decrypt.
        let aes_key = aes::Key::new(key.clone()).unwrap();
        let mut op = match aes.begin_aead(
            aes_key.into(),
            aes::GcmMode::GcmTag12 { nonce: iv.clone().try_into().unwrap() },
            SymmetricOperation::Decrypt,
        ) {
            Ok(c) => c,
            Err(_) => return,
        };
        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut got_pt = op.update(&got_ct[..got_ct.len() - 4]).unwrap();
        got_pt.extend_from_slice(&op.finish().unwrap());
        assert_eq!(test.msg, hex::encode(&got_pt));

        // Corrupted ciphertext should not decrypt.
        let aes_key = aes::Key::new(key).unwrap();
        let mut op = match aes.begin_aead(
            aes_key.into(),
            aes::GcmMode::GcmTag12 { nonce: iv.try_into().unwrap() },
            SymmetricOperation::Decrypt,
        ) {
            Ok(c) => c,
            Err(_) => return,
        };
        op.update_aad(aad.as_ffi_slice()).unwrap();
        let mut corrupt_ct = got_ct.clone();
        corrupt_ct[0] ^= 0x01;
        let _corrupt_pt = op.update(&corrupt_ct).unwrap();
        let result = op.finish();
        assert!(result.is_err());
    }

    info!("test_aes_gcm: Success");
}

/// Test basic [`Des`] functionality.
pub fn test_des<D: Des>(des: D) {
    struct TestCase {
        key: &'static str,
        msg: &'static str,
        ct: &'static str,
    }
    let tests = vec![
        TestCase {
            key: "800000000000000000000000000000000000000000000000",
            msg: "0000000000000000",
            ct: "95a8d72813daa94d",
        },
        TestCase {
            key: "000000000000000000000000000000002000000000000000",
            msg: "0000000000000000",
            ct: "7ad16ffb79c45926",
        },
    ];
    for test in tests {
        let key = hex::decode(test.key).unwrap();
        let msg = hex::decode(test.msg).unwrap();
        let des_key = des::Key::new(key.clone()).unwrap();
        let mut op = des
            .begin(des_key.clone().into(), des::Mode::EcbNoPadding, SymmetricOperation::Encrypt)
            .unwrap();
        let mut got_ct = op.update(&msg).unwrap();
        got_ct.extend_from_slice(&op.finish().unwrap());
        assert_eq!(test.ct, hex::encode(&got_ct));
        let mut op = des
            .begin(des_key.into(), des::Mode::EcbNoPadding, SymmetricOperation::Decrypt)
            .unwrap();
        let mut got_pt = op.update(&got_ct).unwrap();
        got_pt.extend_from_slice(&op.finish().unwrap());
        assert_eq!(test.msg, hex::encode(&got_pt));
    }

    info!("test_des: Success");
}

/// Test [`Rsa`] functionality.
pub fn test_rsa<R: Rsa>(rsa: R) {
    let mut rng = crate::crypto::rng::Rng;

    struct TestCase {
        msg: &'static str,
        ct: &'static str,
    }

    // https://github.com/google/wycheproof/blob/master/testvectors/rsa_pkcs1_2048_test.json
    const PKCS1_DECRYPT_TESTS: &[TestCase] = &[
        // tcId: 1
        TestCase {
            msg: "",
            ct: "5999ccb0cfdd584a3fd9daf247b9cd7314323f8bba4864258f98c6bafc068fe672641bab25ef5b1a7a2b88f67f12af3ca4fe3c493b2062bbb11ad3b1ba0640025c814326ff50ed52b176bd7f606ea9e209bcdcc67c0a0c4b8ed30b9959c57e90fd1efdf99895e2608095f92caff9070dec900fb96d5ce5efd2b2e66b80cff27d482d242b307cb813e7dc818fce31b67ac9a94501b5bc4621b547ba9d81808dd297d600dfc1a7deeb061570cde8894e398453328740adfd77cf76075a109d41ad296651ac817382424a4907d5a342d06cf19c09d5b37a147dd69045bf7d378e19dbbbbfb25282e3d9a4dc9793c8c32ab5a45c0b43dba4daca367b6eb5f4432a62",
        },
        // tcId: 2
        TestCase {
            msg: "0000000000000000000000000000000000000000",
            ct: "a9acec7e58761d9191249ff7ea5db499cadccc51d29f8e7fd0aa2cb9962095626f1cadae29666f04ce2afd4b650be59d071d06446d59107eb508cc60545727b0567dfb4f2f94ca60b939c60be111172f367dfd235516e4a60061648c67f5536650821ac2a60744be3cf6befa8f66e76a3e7c5fbc6dfa4dda55ecbdbffdc98d610de5667a4f485f6168b52bbe470e6014253874ce7b78e509937e0bc5f02857e1ad3cf55139bbe6dc7ac4b1ed5097bf781b7671ca9bb58187aa6c71c58ac0561c5aacf96c35deb24e395b6823de7fc96b8031b5906a34c4dc57e4f1226157b9abd849e1367dda014fbf9ed4ca515a7a04cf87787945007e4f63c0366a5bbc3489",
        },
        // tcId: 8
        TestCase {
            msg: "7878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878",
            ct: "7e151b7b34e3b4abe045cf708640e61501c50fdca629aeca10259d45d15eeec6a2008b6336f57021ac6fdad9a6b29d65f098abff76f93722a8e23fb5e153db075005575dad6adccb7c020cd741c3419946b82d369a07fad5b0d55d51774f8991bd65e9e828d8f5a989c866a024a4a78434e9affd0af2c72f9185d450b627008a8a0968fc6373ca340410306a58921cce1207bb6f6c14e3d1f214304f9f6bb9199909e1610322e834b0ce9f55b1835d7623b82ef548545f984ea51466250159344dde902a0f021ba4baf26b16d8c6a42003f4d5dcae531187dc7e3f87c9e04470599eb623e04fca266e86f98cabb6866004e7fc80b36c3977456e51eb64f4b65f",
        },
    ];

    const PKCS1_DECRYPT_KEY_DER: &'static str = "308204bd020100300d06092a864886f70d0101010500048204a7308204a30201000282010100b3510a2bcd4ce644c5b594ae5059e12b2f054b658d5da5959a2fdf1871b808bc3df3e628d2792e51aad5c124b43bda453dca5cde4bcf28e7bd4effba0cb4b742bbb6d5a013cb63d1aa3a89e02627ef5398b52c0cfd97d208abeb8d7c9bce0bbeb019a86ddb589beb29a5b74bf861075c677c81d430f030c265247af9d3c9140ccb65309d07e0adc1efd15cf17e7b055d7da3868e4648cc3a180f0ee7f8e1e7b18098a3391b4ce7161e98d57af8a947e201a463e2d6bbca8059e5706e9dfed8f4856465ffa712ed1aa18e888d12dc6aa09ce95ecfca83cc5b0b15db09c8647f5d524c0f2e7620a3416b9623cadc0f097af573261c98c8400aa12af38e43cad84d0203010001028201001a502d0eea6c7b69e21d5839101f705456ed0ef852fb47fe21071f54c5f33c8ceb066c62d727e32d26c58137329f89d3195325b795264c195d85472f7507dbd0961d2951f935a26b34f0ac24d15490e1128a9b7138915bc7dbfa8fe396357131c543ae9c98507368d9ceb08c1c6198a3eda7aea185a0e976cd42c22d00f003d9f19d96ea4c9afcbfe1441ccc802cfb0689f59d804c6a4e4f404c15174745ed6cb8bc88ef0b33ba0d2a80e35e43bc90f350052e72016e75b00d357a381c9c0d467069ca660887c987766349fcc43460b4aa516bce079edd87ba164307b752c277ed9528ad3ba0bf1877349ed3b7966a6c240110409bf4d0fade0c68fdadd847fd02818100ec125cf37e310a2ff46263b9b2e0629d6390005ec88913d4fb71bd4dd856124498aaeba983d7ba2bd942e64d223feb7a23af4d605efeea6bd70d39afe99d35a3aa15e74a1768778093be0edd4a8d09b2def6dc9b67ff85764625c2e19236db4c401ce30a2572d3ecb4f969b7ad19c522c02d774465676e1a3776c54d6248348b02818100c2742abcd9897bd4b0b671f973fc82a8f84abf5705ff88dd41948623afe9dca60dc6543390767feaebeb539576ee8bfa61b5fcbca94a7cef75a09150c540fa9694dd8004ad23718c889049219369c99f4458d4afc148f6f07df87324a96d9cf7b385dd8622414a1832f9f29446f050c2d5a6407649dc41ab70e23b3dcc22c9870281810096a9798d250263400bb6277342881627e07cecdf91187b01b89ff47314188a7c20fb24800156d2c85d5666e8df6ceff9f9804ddfad80ff5767de56ecc029c72bf6c717df9f64daafc29acf9dc7908f9a0ad67e20e8949936ccba18d021a2c4febb04349a2b2047c4901385b6e5d0c691d118b33f81802b32ac272ef09e42fad50281800554f41b0b87f68a45722b3be0cf4ab1e165034c1a91002ab8f29e9ef9e2dab6fee7b2455bafb42037e9d2f7e533f348a147412fd72080be7c2633f5d802c91c39e6bcece3e675e59995033c55737020dad9e8b30d04b828adfb9304ad54a11a35a4f50709876ac5b118236ba76a4d7c9a291dd9607b169de1d182385691999f0281801c640189d9bfe8c623833210a76c420c6f44e5d760e259916cec2ae2b156456960fd95e2747660c389562250f055049cfab7e5c3039549384a7a2aaeb1c824d3af709482a8cf9b587022a00b1f0722db50f33cb26dc20dd2245d5265df61ee2983c938c2167dcee121fc4b4479c237e728cf633ab60a8c0ecd04fce7e3baa559";

    let der = hex::decode(PKCS1_DECRYPT_KEY_DER).unwrap();
    let pkinfo = pkcs8::PrivateKeyInfo::try_from(der.as_slice()).unwrap();
    let key = rsa::Key(pkinfo.private_key.to_vec());

    for (i, test) in PKCS1_DECRYPT_TESTS.iter().enumerate() {
        info!("test_rsa: PKCS#1 decryption test {}", i);
        let mut op = rsa
            .begin_decrypt(OpaqueOr::Explicit(key.clone()), rsa::DecryptionMode::Pkcs1_1_5Padding)
            .unwrap();
        op.update(hex::decode(test.ct).unwrap().as_slice()).unwrap();
        let res = op.finish().unwrap();
        assert_eq!(hex::decode(test.msg).unwrap(), res);
    }

    // https://github.com/google/wycheproof/blob/master/testvectors/rsa_oaep_2048_sha256_mgf1sha256_test.json
    const OAEP_DECRYPT_TESTS: &[TestCase] = &[
        // tcId: 1
        TestCase {
            msg: "",
            ct: "6e62bf24d95aff6868afec2a92a445b6458f16f688c19fe1212f66a63137831653cedd359d8cff4dd485d77dfd55812c181373201f54aafd65730d2a304e623455d51125d891e65d97fce52341cae45fb64c38a384a1c621e2713ee6794633f029a9fd4d774f56551eac2176162e162640f25eab873a3451c475570f19228bcede4c67c370a75ed7fabccd538c9819eff182481b10d42f1a9f6a05373b8cf9b71818d467bd3b8ebacb619e8ad42916e600c043effceb3855bc48a629e60ae886f51b2a7876b0e623fb2ce68af4b039242f963adb0e4240aed0ed07f65f1ee7c0cc77d210d0c2d1dc10c81b881aa0c9c9e9499665cf2970d2ccfeeb3191531765",
        },
        // tcId: 2
        TestCase {
            msg: "0000000000000000000000000000000000000000",
            ct: "207180c340658b5154ae45d2e4e7326a0997c683a26b595e536a29333c4b66149af85e029d5419a39e3a147b221516ffd86b6b4b66c3e0c4c49fe8c57a2f5c37b8704b9b592b80db9cd788a4ed51ab4f0a1cbed63bd18d1f06a22f225866b0c2c417cb23473b7ba4250b1353bd2e5b4f0f937cd2efe5fa38db3c295f7748b970088657db4aa9a76e1ee6fbff166ec1861d00d085326c7384bdd1bc2f400d4f74dbdfadaf3fdc46073e668573e02030b9eb5af58eb540c66677a771194479ec0098d858a2ea45d0ba1e6b32440dfbac745000554d51a17684ca964b02a74d479f1d432ef763ef4059715a4348cfe36a215359712f25b6977903be4adb92febbf6",
        },
        // tcId: 7
        TestCase {
            msg: "e0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff",
            ct: "096958786ee7972050d67a9e4b69d6c6af7db7cc674386df725770dd29129b826e39552330104c8d71e6cc3a3014dd2f61b54153af51b0438d447ee939f9e3c13bb8b00a37dea6a068f6c9d27e848b1be7a1eeeb3ee50b78036fba95ae46948ca5b13f356ea24db10f60dc09e4b8bad8f766b668ef72524432080a0ce00ed676d6d5e354984b1078520412525848156d06f0652469f95791baa3d9a798ae537094f76f976faecd5c9ce0c930a75910c63dacf63485cb4b5e7bdbcf4d80e74037eaa1a8fe4b52930bec6be99cf6ac88cf5878dbf6859d456a95dbc34654eec425de84ca2a535d517403a9aada827e7d0093ecfc97ed056a7652825e9a45cb2dcb",
        },
        // tdId: 11
        TestCase {
            msg: "78787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878",
            ct: "7efb69f1137d6a6e813b7ab75bf0400b3d07a442b88ab048675dc06b0215fc1a2e033263ec31a6c1d2eac56cb0470d69022a48131d1f000bbed70586b80cf6356465c8834daced7ea2a5ff8ef9c44d5ab828ffbf0556a6394752a4a28a70cae20084e1236f042f6c29de5cb34ef73acba5abcc7ccb3a26342701df3b9daa945d9fa5bf0b9b10306655e56370183f50fb8321f8f0cd1c72114791fca5df2166296b509b01a0b291c46110787cebe69d41b3b1e89590bc2f5e5d49ea24ae0f1207eef1aee54b9760553e80c5506a8a8a75732e92875025f0bfd5ead71e4340c8a9fa16dcd5a7dc96d8c4a7dc4e91f47a69366445c4695c8bad578ffe52bb672f65",
        },
    ];

    const OAEP_DECRYPT_KEY_DER: &'static str = "308204bd020100300d06092a864886f70d0101010500048204a7308204a30201000282010100a2b451a07d0aa5f96e455671513550514a8a5b462ebef717094fa1fee82224e637f9746d3f7cafd31878d80325b6ef5a1700f65903b469429e89d6eac8845097b5ab393189db92512ed8a7711a1253facd20f79c15e8247f3d3e42e46e48c98e254a2fe9765313a03eff8f17e1a029397a1fa26a8dce26f490ed81299615d9814c22da610428e09c7d9658594266f5c021d0fceca08d945a12be82de4d1ece6b4c03145b5d3495d4ed5411eb878daf05fd7afc3e09ada0f1126422f590975a1969816f48698bcbba1b4d9cae79d460d8f9f85e7975005d9bc22c4e5ac0f7c1a45d12569a62807d3b9a02e5a530e773066f453d1f5b4c2e9cf7820283f742b9d50203010001028201007627eef3567b2a27268e52053ecd31c3a7172ccb9ddcee819b306a5b3c66b7573ca4fa88efc6f3c4a00bfa0ae7139f64543a4dac3d05823f6ff477cfcec84fe2ac7a68b17204b390232e110310c4e899c4e7c10967db4acde042dbbf19dbe00b4b4741de1020aaaaffb5054c797c9f136f7d93ac3fc8caff6654242d7821ebee517bf537f44366a0fdd45ae05b9909c2e6cc1ed9281eff4399f76c96b96233ec29ae0bbf0d752b234fc197389f51050aa1acd01c074c3ac8fbdb9ea8b651a95995e8db4ad5c43b6c8673e5a126e7ee94b8dff4c5afc01259bc8da76950bae6f8bae715f50985b0d6f66d04c6fef3b700720eecdcdf171bb7b1ecbe7289c467c102818100dc431050f782e894fb5248247d98cb7d58b8d1e24f3b55d041c56e4de086b0d5bb028bda42eeb5d234d5681e5809d415e6a289ad4cfbf78f978f6c35814f50eebff1c5b80a69f788e81e6bab5ddaa78369d659d143ec6f17e79813a575cfad9c569156b90113e2e9110ad9e7b48a1c9348a6e653321191290ea36cfb3a5b18f102818100bd1a81e7977f9898122273ae3222b598ea5fb19eb4eabc38308a5e32196603b2e500ffb79f5b886816611debc472fac45544070beb057c941378a6868af3b7a03d3f9880ec47d5e089b94fbde542aba9ae8d72c57088d7abf5b131f39098f7bc160f90536abc9492fd4e06f3ed7299d4b97bb03677207d95669f140cfbc20f2502818100a94b528b28f291599121d91952ffd1c7f21d7c1479d99d478885fb161870ee1218bf08472612dbe5497e8d9c650688e09c786961ae3e2c354dc48ae34514759c4c23c4588488961dc06b414e61c0e1e7fbbd2923d31532fe289f96da220711e58c14019808e00414276933bb07e4efb9b4a9b37656917205209f33f09515d7c10281803af0e72a933aef09ff2503df78bafed531c02ff1a2bc437c540cdcbd4ad35435cf511763596543480629b114ca7f780ff7efa32ea0cb6e000d6d9ea1f2ef71fd9cf9948422a165557e37e755edfe70d90b920502eb478bc98a63f788ce3a0f856d6ede7251a383bfa8fa480a81a925af7b3cc538c4bab8c9f7597ffb68011d8d0281802640fbfbcfefb163ee7a87b6483a66ee41f956d90fa8a7939bfc042ee0924b1b7993d0445f758d51933e85179c0320b0c968b48a91c38b5be923e1097c0c562f88d42294b6a2759bafa5428a74f1270874e45f6fcc60f21602de5eccd143cf31241f5921b5ad3983fb54ef17be3b285367e50c999c67247b552fe4bfce945f7b";

    let der = hex::decode(OAEP_DECRYPT_KEY_DER).unwrap();
    let pkinfo = pkcs8::PrivateKeyInfo::try_from(der.as_slice()).unwrap();
    let key = rsa::Key(pkinfo.private_key.to_vec());

    for (i, test) in OAEP_DECRYPT_TESTS.iter().enumerate() {
        info!("test_rsa: OAEP decryption test (SHA-256) {}", i);
        let mut op = rsa
            .begin_decrypt(
                OpaqueOr::Explicit(key.clone()),
                rsa::DecryptionMode::OaepPadding {
                    msg_digest: Digest::Sha256,
                    mgf_digest: Digest::Sha256,
                },
            )
            .unwrap();
        op.update(hex::decode(test.ct).unwrap().as_slice()).unwrap();
        let res = op.finish().unwrap();
        assert_eq!(hex::decode(test.msg).unwrap(), res);
    }

    // https://github.com/C2SP/wycheproof/blob/master/testvectors/rsa_oaep_2048_sha256_mgf1sha1_test.json
    const OAEP2_DECRYPT_TESTS: &[TestCase] = &[
        // tcId: 1
        TestCase {
            msg: "",
            ct: "8e6f127b86ed4ce03bea0242759dec562f3c0e475d70c950bb9865c5a00c19186487f6dad25e6ed4600510e067a8679cdd63f7718af92e5cc297d74d5ce72472c404083b156924c39852b03fad90becc3da0cbb1e80556b4010e9569c61e3b188b9dbbf58f779d3be5a9a7d000ab596d69c9aa48fa6c1f1fbc5be79ec39e27b7a76191b681a02d61cbc5924651198bdf9bb7749ab4a515d1ea1d9d32dad38dc703228985985043c152e2d8e918b652d67a40c2be1e2c6cc2fce11f6c923714b11732d8fdb1613c46bfcecafd64f9536fb7b41816736e3e4b62a1dd6e4c26e8a8f66d99cced308127a39ea1f21a6d7886e22aabf3ca6d6464278d930bf60f277f",
        },
        // tcId: 2
        TestCase {
            msg: "0000000000000000000000000000000000000000",
            ct: "7a896725e0944db789d4caa96bd8701fdc100a26ca12e45d7d9a5f5599fdec0a8ca5ba9e2e0a5c743d2e82a0006b915e6572e066c30bd794e98fe0959519f418d5587a5012ff9b0c545930e3065cf8deac440ef60715ecb8de63f2bea7fb80bc81cefd2f5b979bf0d32e07e615db6a363f0447bec068db90a9e86bb4703098b3ddf1bc34b2803930b56fbb8e026b8691248d8e471ed6dc0a90905f96412470f1002ab2a754cda6dead97c5a05fbcec5d0398c1561876bac021ec4cdc6915d929ebb6fa5ffa1d6e37db99951fa19670ae0f8bbe18bacab54bebeb5ec2dfb4a0cf69dd7077b3229fd0cd8580fb56fa13c399364e2bdecc1a0bfc6eb67f01a17fcc",
        },
        // tcId: 7
        TestCase {
            msg: "e0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff",
            ct: "76f7b6e1cf45b005ad58b5354cfc5799f74edfb27f27b414b4d25500a1ec4bcd46c6b65603b204f69a2a71b8d1099ce96c8ee52e119ed9b080d86d82789e3e5777cc5f920b147126ec8612b206bc5734e828ac819f90ea7191832d570d376df2c4e3eb5070fd8382f8c0a9b89da928bfbdd24bf1d17ebc83f9237a51352ff04b6bb3848cb6a9c195e5369f4b6ed9b4cc166377f88c7e6db6ef78c0e1bfbac5a3825867af9b22689b627dcd8d1441b515b15b78688b52b04df4157a888aabdb9e792c65fcbdcd03743fe45e637afa7e422782e6da58b95163acd59353e634337abb1c15b831a9dec79c517a5be0b4ee43f7544a2e9bf6af2bc53b080c60dc2bc6",
        },
        // tdId: 11
        TestCase {
            msg: "78787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878787878",
            ct: "9edf0e6ae25a3e7b6f31fa13a7a31db0e6ce2faa624c2ebcba9669bf5f205758e1e904a9a738bdf430072b0563d1a87f16522811d645cd9f85f13359d2961bbc583a3b15c66ab5a0d1373c2949261e3f44c2a1a88e25190efd30aa9fc410d2d23148c16daa90457bf1ee77c2d344525036e037ed473275bd535fa961a66e47072b586915d85d3d7edaae7945b5e7f08fa15a4d08cc669b3846b1ea02cdc3eb9bc5a54aba227f9434f19d28d06add791fce9efe2171e2c676bc1b09bc163798a1a74b7fdedad993dc47c8323cbe3aead96d0d4e7e494ac390200e6a79f96d88844d59a3d78626b0797b902220874e6957e6c214f3ab7574c6e476cc4262beda98",
        },
    ];

    let der = hex::decode(OAEP_DECRYPT_KEY_DER).unwrap();
    let pkinfo = pkcs8::PrivateKeyInfo::try_from(der.as_slice()).unwrap();
    let key = rsa::Key(pkinfo.private_key.to_vec());

    for (i, test) in OAEP2_DECRYPT_TESTS.iter().enumerate() {
        info!("test_rsa: OAEP decryption test (SHA-256 + MGF1-SHA1) {}", i);
        let mut op = rsa
            .begin_decrypt(
                OpaqueOr::Explicit(key.clone()),
                rsa::DecryptionMode::OaepPadding {
                    msg_digest: Digest::Sha256,
                    mgf_digest: Digest::Sha1,
                },
            )
            .unwrap();
        op.update(hex::decode(test.ct).unwrap().as_slice()).unwrap();
        let res = op.finish().unwrap();
        assert_eq!(hex::decode(test.msg).unwrap(), res);
    }

    // https://github.com/google/wycheproof/blob/master/testvectors/rsa_sig_gen_misc_test.json
    const PKCS1_SIGN_KEY_DER: &'static str = "308204bd020100300d06092a864886f70d0101010500048204a7308204a30201000282010100a2b451a07d0aa5f96e455671513550514a8a5b462ebef717094fa1fee82224e637f9746d3f7cafd31878d80325b6ef5a1700f65903b469429e89d6eac8845097b5ab393189db92512ed8a7711a1253facd20f79c15e8247f3d3e42e46e48c98e254a2fe9765313a03eff8f17e1a029397a1fa26a8dce26f490ed81299615d9814c22da610428e09c7d9658594266f5c021d0fceca08d945a12be82de4d1ece6b4c03145b5d3495d4ed5411eb878daf05fd7afc3e09ada0f1126422f590975a1969816f48698bcbba1b4d9cae79d460d8f9f85e7975005d9bc22c4e5ac0f7c1a45d12569a62807d3b9a02e5a530e773066f453d1f5b4c2e9cf7820283f742b9d50203010001028201007627eef3567b2a27268e52053ecd31c3a7172ccb9ddcee819b306a5b3c66b7573ca4fa88efc6f3c4a00bfa0ae7139f64543a4dac3d05823f6ff477cfcec84fe2ac7a68b17204b390232e110310c4e899c4e7c10967db4acde042dbbf19dbe00b4b4741de1020aaaaffb5054c797c9f136f7d93ac3fc8caff6654242d7821ebee517bf537f44366a0fdd45ae05b9909c2e6cc1ed9281eff4399f76c96b96233ec29ae0bbf0d752b234fc197389f51050aa1acd01c074c3ac8fbdb9ea8b651a95995e8db4ad5c43b6c8673e5a126e7ee94b8dff4c5afc01259bc8da76950bae6f8bae715f50985b0d6f66d04c6fef3b700720eecdcdf171bb7b1ecbe7289c467c102818100dc431050f782e894fb5248247d98cb7d58b8d1e24f3b55d041c56e4de086b0d5bb028bda42eeb5d234d5681e5809d415e6a289ad4cfbf78f978f6c35814f50eebff1c5b80a69f788e81e6bab5ddaa78369d659d143ec6f17e79813a575cfad9c569156b90113e2e9110ad9e7b48a1c9348a6e653321191290ea36cfb3a5b18f102818100bd1a81e7977f9898122273ae3222b598ea5fb19eb4eabc38308a5e32196603b2e500ffb79f5b886816611debc472fac45544070beb057c941378a6868af3b7a03d3f9880ec47d5e089b94fbde542aba9ae8d72c57088d7abf5b131f39098f7bc160f90536abc9492fd4e06f3ed7299d4b97bb03677207d95669f140cfbc20f2502818100a94b528b28f291599121d91952ffd1c7f21d7c1479d99d478885fb161870ee1218bf08472612dbe5497e8d9c650688e09c786961ae3e2c354dc48ae34514759c4c23c4588488961dc06b414e61c0e1e7fbbd2923d31532fe289f96da220711e58c14019808e00414276933bb07e4efb9b4a9b37656917205209f33f09515d7c10281803af0e72a933aef09ff2503df78bafed531c02ff1a2bc437c540cdcbd4ad35435cf511763596543480629b114ca7f780ff7efa32ea0cb6e000d6d9ea1f2ef71fd9cf9948422a165557e37e755edfe70d90b920502eb478bc98a63f788ce3a0f856d6ede7251a383bfa8fa480a81a925af7b3cc538c4bab8c9f7597ffb68011d8d0281802640fbfbcfefb163ee7a87b6483a66ee41f956d90fa8a7939bfc042ee0924b1b7993d0445f758d51933e85179c0320b0c968b48a91c38b5be923e1097c0c562f88d42294b6a2759bafa5428a74f1270874e45f6fcc60f21602de5eccd143cf31241f5921b5ad3983fb54ef17be3b285367e50c999c67247b552fe4bfce945f7b";

    const PKCS1_SIGN_TESTS: &[TestCase] = &[
        // tcId: 81
        TestCase {
            msg: "",
            ct: "840f5dac53106dd1f9c57219224cf51289290c42f20466875ba8e830ac5690e541536fcc8ab03b731f82bf66d83f194e7e180b3963ec7a2f3f7904a7ce49aed47da4d4b79421eaf937d301b3e696169297b797c32c076a12be4de0b58e003c5123051a84a10c62f8dac2f42a8640008eb3c7cccd6760ff5b51b689763922582845f048fb8150e5a7a6ca2eccc7bdc85349ad5b26c52137a79fa3fe5c29ab5cd7615013219c1941b6708e9c3c23feff5febaf0c8ebca5750b54e3e6e99a3e876b396f27860b7f3ec4e9191703c6332d944f6f69751167680c79c4f6b57f1cc8755d24b6ec158ccdbacdb23107a33cb6b332516c13274d1f9dccc21dced869e486",
        },
        // tcId: 82
        TestCase {
            msg: "0000000000000000000000000000000000000000",
            ct: "8a1b220cb2ab415dc760eb7f5bb10335a3cca269d7dbbf7d0962ba79f9cf7b43a5fc09c99a1584f07403473d6c189a836897a5b6f8ea9fa22d601e6ba5f7411fe27c638b81b1a22363583a80fce8c7df3e40fb51bd0e60d0a6653f79f3bcb7ec3e9dc14cfb5b31ab1735bca692d50ac03f979dda92747c6430f8045efa3513ba6e0ce3e9e35570e1c30c8ebe589b44192e1344ca83dfa576fc6fdc7bf1cd7cee875b001c8c02ce8d602769e4bd9d241c4857182a0089a8b67644e73eef105c550efa47a40874289395ac0c4e02fd4ba98e130a4c2d1b95521c6af4a002ac3bdc6e52122ae4c08cc3da1c896e059acbddec574ac0432f6103dd97273d8803c102",
        },
        // tcId: 88
        TestCase {
            msg: "0102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f606162636465666768696a6b6c6d6e6f707172737475767778797a7b7c7d7e7f808182838485868788898a8b8c8d8e8f909192939495969798999a9b9c9d9e9fa0a1a2a3a4a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebfc0c1c2c3c4c5c6c7c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadbdcdddedfe0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff000102030405060708090a0b0c0d0e0f1011121314151617",
            ct: "7aad44a36610ac147835efc623e3aeec0d5d8acbd7f469f92142592c7b843c9326e2015c4bf3843678d2e183ec9ed568e5dd8d535ea77a6d7fe804222e6208d0160bd6cf2744cdb56bce0ed7269cc5f2bcc25d3474c0fb5bc7d20ebf3664bad858dc6e86dabfa5f39a70e23344ab4f8d5edc6397d9d1b54fda4216e0b93d37b906384f82d36666d526939e0f917344208aadf05416c656a11a307ce2101912763728cfc0bd237017d36b8566c6c366b13f142c93edde181146ec63e49a57335b5d9295b85aa4c00d49cae7930653a5651c21371a4b3ec8a6e0f371d005e8b4f1631f7466b767b4789e75e1d2bc63ce4c46e5e7baf0b801ef785fd07ae79bbeef",
        },
    ];

    let der = hex::decode(PKCS1_SIGN_KEY_DER).unwrap();
    let pkinfo = pkcs8::PrivateKeyInfo::try_from(der.as_slice()).unwrap();
    let key = rsa::Key(pkinfo.private_key.to_vec());

    for (i, test) in PKCS1_SIGN_TESTS.iter().enumerate() {
        info!("test_rsa: PKCS#1 v1.5 signing test {}", i);
        let mut op = rsa
            .begin_sign(
                OpaqueOr::Explicit(key.clone()),
                rsa::SignMode::Pkcs1_1_5Padding(Digest::Sha256),
            )
            .unwrap();
        op.update(hex::decode(test.msg).unwrap().as_slice()).unwrap();
        let res = op.finish().unwrap();
        assert_eq!(hex::decode(test.ct).unwrap(), res);
    }

    info!("test_rsa: Generating a key");
    match rsa
        .generate_key(&mut rng, kmr_wire::KeySizeInBits(2048), kmr_wire::RsaExponent(65537), &[])
        .unwrap()
    {
        KeyMaterial::Rsa(ref rsakey) => {
            let key = kmr_common::explicit!(rsakey).unwrap();
            let key = pkcs1::RsaPrivateKey::try_from(key.0.as_slice()).unwrap();
            assert_eq!(key.public_exponent.as_bytes(), [1, 0, 1]); // 65537

            info!("test_rsa: PSS signing test");
            let mut op =
                rsa.begin_sign(rsakey.clone(), rsa::SignMode::PssPadding(Digest::Sha512)).unwrap();
            op.update("00000000000000000000".as_bytes()).unwrap();
            let sig1 = op.finish().unwrap();
            let mut op =
                rsa.begin_sign(rsakey.clone(), rsa::SignMode::PssPadding(Digest::Sha512)).unwrap();
            op.update("00000000000000000000".as_bytes()).unwrap();
            let sig2 = op.finish().unwrap();
            assert_ne!(sig1, sig2);
        }
        _ => {
            error!("test_rsa: Failed to generate a key!");
        }
    }

    info!("test_rsa: Success");
}

fn kgi_to_str(info: &KeyGenInfo) -> &'static str {
    match info {
        KeyGenInfo::NistEc(curve) => match curve {
            ec::NistCurve::P224 => "NIST P224",
            ec::NistCurve::P256 => "NIST P256",
            ec::NistCurve::P384 => "NIST P384",
            ec::NistCurve::P521 => "NIST P521",
        },
        KeyGenInfo::Ed25519 => "Ed25519",
        KeyGenInfo::X25519 => "X25519",
        _ => "Unknown",
    }
}

fn test_ec_generate<E: Ec>(ec: &E, info: &KeyGenInfo) -> Result<ec::Key, kmr_common::Error> {
    let mut rng = crate::crypto::rng::Rng;

    info!("test_ec: {}: Generating a key", kgi_to_str(info));

    let keymaterial = match info {
        KeyGenInfo::NistEc(curve) => ec.generate_nist_key(&mut rng, curve.clone(), &[]),
        KeyGenInfo::Ed25519 => ec.generate_ed25519_key(&mut rng, &[]),
        KeyGenInfo::X25519 => ec.generate_x25519_key(&mut rng, &[]),
        _ => {
            Err(kmr_common::km_err!(UnsupportedKeyFormat, "test_ec_generate: Unsupported key type"))
        }
    }?;

    match keymaterial {
        KeyMaterial::Ec(curve, _curve_type, key) => {
            let key = kmr_common::explicit!(key)?;
            let _ = match &key {
                ec::Key::P224(key)
                | ec::Key::P256(key)
                | ec::Key::P384(key)
                | ec::Key::P521(key) => ec.nist_public_key(&key, ec::NistCurve::try_from(curve)?),
                ec::Key::Ed25519(key) => ec.ed25519_public_key(&key),
                ec::Key::X25519(key) => ec.x25519_public_key(&key),
            }?;

            Ok(key)
        }
        _ => Err(kmr_common::km_err!(UnsupportedEcCurve, "test_ec: Unexpected key type")),
    }
}

fn test_ec_derive<E: Ec>(
    ec: &E,
    key1: ec::Key,
    key2: ec::Key,
) -> Result<Vec<u8>, kmr_common::Error> {
    let mut op = ec.begin_agree(OpaqueOr::Explicit(key1))?;
    let mut buf = Vec::<u8>::new();
    let curve = key2.curve();
    let curve_type = key2.curve_type();
    let key2 = OpaqueOr::Explicit(key2);
    let spki = key2.subject_public_key_info(&mut buf, ec, &curve, &curve_type)?;
    let der: der::Document = spki.ref_to_owned().try_into().unwrap();
    let _ = op.update(der.as_bytes())?;
    Ok(op.finish()?)
}

fn test_ec_sign<E: Ec>(ec: &E, key: ec::Key, digest: Digest) -> Result<(), kmr_common::Error> {
    let mut op = ec.begin_sign(OpaqueOr::Explicit(key.clone()), digest).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    let sig1 = op.finish().unwrap();
    let mut op = ec.begin_sign(OpaqueOr::Explicit(key.clone()), digest).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    op.update("00000000000000000000".as_bytes()).unwrap();
    let sig2 = op.finish().unwrap();
    if key.curve_type() == CurveType::Nist {
        // EC-DSA signatures are randomized
        assert_ne!(sig1, sig2);
    } else {
        // Ed25519 signatures are deterministic
        assert_eq!(sig1, sig2);
    }
    Ok(())
}

fn test_ec_ed25519<E: Ec>(ec: &E) {
    struct TestCase {
        secret_key: &'static str,
        public_key: &'static str,
        msg: &'static str,
        sig: &'static str,
    }

    // https://datatracker.ietf.org/doc/html/rfc8032#section-7.1
    const TESTS: &[TestCase] = &[
        TestCase {
            secret_key: "9d61b19deffd5a60ba844af492ec2cc44449c5697b326919703bac031cae7f60",
            public_key: "d75a980182b10ab7d54bfed3c964073a0ee172f3daa62325af021a68f707511a",
            msg: "",
            sig: "e5564300c360ac729086e2cc806e828a84877f1eb8e5d974d873e065224901555fb8821590a33bacc61e39701cf9b46bd25bf5f0595bbe24655141438e7a100b",
        },
        TestCase {
            secret_key: "4ccd089b28ff96da9db6c346ec114e0f5b8a319f35aba624da8cf6ed4fb8a6fb",
            public_key: "3d4017c3e843895a92b70aa74d1b7ebc9c982ccf2ec4968cc0cd55f12af4660c",
            msg: "72",
            sig: "92a009a9f0d4cab8720e820b5f642540a2b27b5416503f8fb3762223ebdb69da085ac1e43e15996e458f3613d0f11d8c387b2eaeb4302aeeb00d291612bb0c00",
        },
        TestCase {
            secret_key: "c5aa8df43f9f837bedb7442f31dcb7b166d38535076f094b85ce3a2e0b4458f7",
            public_key: "fc51cd8e6218a1a38da47ed00230f0580816ed13ba3303ac5deb911548908025",
            msg: "af82",
            sig: "6291d657deec24024827e69c3abe01a30ce548a284743a445e3680d7db5ac3ac18ff9b538d16f290ae67f760984dc6594a7c15e9716ed28dc027beceea1ec40a",
        },
        TestCase {
            secret_key: "f5e5767cf153319517630f226876b86c8160cc583bc013744c6bf255f5cc0ee5",
            public_key: "278117fc144c72340f67d0f2316e8386ceffbf2b2428c9c51fef7c597f1d426e",
            msg: "08b8b2b733424243760fe426a4b54908632110a66c2f6591eabd3345e3e4eb98fa6e264bf09efe12ee50f8f54e9f77b1e355f6c50544e23fb1433ddf73be84d879de7c0046dc4996d9e773f4bc9efe5738829adb26c81b37c93a1b270b20329d658675fc6ea534e0810a4432826bf58c941efb65d57a338bbd2e26640f89ffbc1a858efcb8550ee3a5e1998bd177e93a7363c344fe6b199ee5d02e82d522c4feba15452f80288a821a579116ec6dad2b3b310da903401aa62100ab5d1a36553e06203b33890cc9b832f79ef80560ccb9a39ce767967ed628c6ad573cb116dbefefd75499da96bd68a8a97b928a8bbc103b6621fcde2beca1231d206be6cd9ec7aff6f6c94fcd7204ed3455c68c83f4a41da4af2b74ef5c53f1d8ac70bdcb7ed185ce81bd84359d44254d95629e9855a94a7c1958d1f8ada5d0532ed8a5aa3fb2d17ba70eb6248e594e1a2297acbbb39d502f1a8c6eb6f1ce22b3de1a1f40cc24554119a831a9aad6079cad88425de6bde1a9187ebb6092cf67bf2b13fd65f27088d78b7e883c8759d2c4f5c65adb7553878ad575f9fad878e80a0c9ba63bcbcc2732e69485bbc9c90bfbd62481d9089beccf80cfe2df16a2cf65bd92dd597b0707e0917af48bbb75fed413d238f5555a7a569d80c3414a8d0859dc65a46128bab27af87a71314f318c782b23ebfe808b82b0ce26401d2e22f04d83d1255dc51addd3b75a2b1ae0784504df543af8969be3ea7082ff7fc9888c144da2af58429ec96031dbcad3dad9af0dcbaaaf268cb8fcffead94f3c7ca495e056a9b47acdb751fb73e666c6c655ade8297297d07ad1ba5e43f1bca32301651339e22904cc8c42f58c30c04aafdb038dda0847dd988dcda6f3bfd15c4b4c4525004aa06eeff8ca61783aacec57fb3d1f92b0fe2fd1a85f6724517b65e614ad6808d6f6ee34dff7310fdc82aebfd904b01e1dc54b2927094b2db68d6f903b68401adebf5a7e08d78ff4ef5d63653a65040cf9bfd4aca7984a74d37145986780fc0b16ac451649de6188a7dbdf191f64b5fc5e2ab47b57f7f7276cd419c17a3ca8e1b939ae49e488acba6b965610b5480109c8b17b80e1b7b750dfc7598d5d5011fd2dcc5600a32ef5b52a1ecc820e308aa342721aac0943bf6686b64b2579376504ccc493d97e6aed3fb0f9cd71a43dd497f01f17c0e2cb3797aa2a2f256656168e6c496afc5fb93246f6b1116398a346f1a641f3b041e989f7914f90cc2c7fff357876e506b50d334ba77c225bc307ba537152f3f1610e4eafe595f6d9d90d11faa933a15ef1369546868a7f3a45a96768d40fd9d03412c091c6315cf4fde7cb68606937380db2eaaa707b4c4185c32eddcdd306705e4dc1ffc872eeee475a64dfac86aba41c0618983f8741c5ef68d3a101e8a3b8cac60c905c15fc910840b94c00a0b9d0",
            sig: "0aab4c900501b3e24d7cdf4663326a3a87df5e4843b2cbdb67cbf6e460fec350aa5371b1508f9f4528ecea23c436d94b5e8fcd4f681e30a6ac00a9704a188a03",
        },
        TestCase {
            secret_key: "833fe62409237b9d62ec77587520911e9a759cec1d19755b7da901b96dca3d42",
            public_key: "ec172b93ad5e563bf4932c70e1245034c35467ef2efd4d64ebf819683467e2bf",
            msg: "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f",
            sig: "dc2a4459e7369633a52b1bf277839a00201009a3efbf3ecb69bea2186c26b58909351fc9ac90b3ecfdfbc7c66431e0303dca179c138ac17ad9bef1177331a704",
        },
    ];

    for (i, test) in TESTS.iter().enumerate() {
        info!("test_ec: Ed25519 signing test {}", i);
        let key = ec::Ed25519Key(hex::decode(test.secret_key).unwrap().try_into().unwrap());
        assert_eq!(hex::decode(test.public_key).unwrap(), ec.ed25519_public_key(&key).unwrap());
        let mut op =
            ec.begin_sign(OpaqueOr::Explicit(ec::Key::Ed25519(key)), Digest::None).unwrap();
        op.update(hex::decode(test.msg).unwrap().as_slice()).unwrap();
        let sig = op.finish().unwrap();
        assert_eq!(sig, hex::decode(test.sig).unwrap());
    }
}

fn test_ec_x25519<E: Ec>(ec: &E) {
    struct TestCase {
        secret_key: &'static str,
        peer_key: &'static str,
        shared_secret: &'static str,
    }

    // https://github.com/google/wycheproof/blob/master/testvectors/x25519_test.json
    const TESTS: &[TestCase] = &[
        // tcId: 1
        TestCase {
            secret_key: "c8a9d5a91091ad851c668b0736c1c9a02936c0d3ad62670858088047ba057475",
            peer_key: "504a36999f489cd2fdbc08baff3d88fa00569ba986cba22548ffde80f9806829",
            shared_secret: "436a2c040cf45fea9b29a0cb81b1f41458f863d0d61b453d0a982720d6d61320",
        },
        // tcId: 2
        TestCase {
            secret_key: "d85d8c061a50804ac488ad774ac716c3f5ba714b2712e048491379a500211958",
            peer_key: "63aa40c6e38346c5caf23a6df0a5e6c80889a08647e551b3563449befcfc9733",
            shared_secret: "279df67a7c4611db4708a0e8282b195e5ac0ed6f4b2f292c6fbd0acac30d1332",
        },
        // tcId: 11
        TestCase {
            secret_key: "a8c9df5820eb399d471dfa3215d96055b3c7d0f4ea49f8ab028d6a6e3194517b",
            peer_key: "0000010000000000000000000000000000000000000000000000000000000000",
            shared_secret: "a92a96fa029960f9530e6fe37e2429cd113be4d8f3f4431f8546e6c76351475d",
        },
        // tcId: 34
        TestCase {
            secret_key: "a8386f7f16c50731d64f82e6a170b142a4e34f31fd7768fcb8902925e7d1e25a",
            peer_key: "0400000000000000000000000000000000000000000000000000000000000000",
            shared_secret: "34b7e4fa53264420d9f943d15513902342b386b172a0b0b7c8b8f2dd3d669f59",
        },
        // tcId: 56
        TestCase {
            secret_key: "881874fda3a99c0f0216e1172fbd07ab1c7df78602cc6b11264e57aab5f23a49",
            peer_key: "edfffffffffffffffffffffffffffffffffffffffffffffffeffffffffffff7f",
            shared_secret: "e417bb8854f3b4f70ecea557454c5c4e5f3804ae537960a8097b9f338410d757",
        },
    ];

    for (i, test) in TESTS.iter().enumerate() {
        info!("test_ec: X25519 key agreement test {}", i);
        let key = ec::X25519Key(hex::decode(test.secret_key).unwrap().try_into().unwrap());
        let der: der::Document = SubjectPublicKeyInfoRef {
            algorithm: AlgorithmIdentifier { oid: ec::X509_X25519_OID, parameters: None },
            subject_public_key: BitStringRef::from_bytes(
                hex::decode(test.peer_key).unwrap().as_slice(),
            )
            .unwrap(),
        }
        .try_into()
        .unwrap();
        let mut op = ec.begin_agree(OpaqueOr::Explicit(ec::Key::X25519(key))).unwrap();
        op.update(der.as_bytes()).unwrap();
        let shared_secret = op.finish().unwrap();
        assert_eq!(shared_secret, hex::decode(test.shared_secret).unwrap());
    }
}

fn test_ec_alg<E: Ec>(ec: &E, info: &KeyGenInfo) {
    let key1 = test_ec_generate(ec, info).unwrap();

    match info {
        KeyGenInfo::NistEc(_) | KeyGenInfo::X25519 => {
            let key2 = test_ec_generate(ec, info).unwrap();
            info!("test_ec: {} key agreement test", kgi_to_str(info));
            let secret1 = test_ec_derive(ec, key1.clone(), key2.clone()).unwrap();
            let secret2 = test_ec_derive(ec, key2.clone(), key1.clone()).unwrap();
            assert_eq!(secret1, secret2);
        }
        _ => {
            test_ec_derive(ec, key1.clone(), key1.clone())
                .expect_err("Key agreement succeeded for unsupported key type?");
        }
    }

    match info {
        KeyGenInfo::NistEc(ec::NistCurve::P224) => {
            info!("test_ec: {} signing test", kgi_to_str(info));
            let _ = test_ec_sign(ec, key1.clone(), Digest::Sha224).unwrap();
        }
        KeyGenInfo::NistEc(ec::NistCurve::P256) => {
            info!("test_ec: {} signing test", kgi_to_str(info));
            let _ = test_ec_sign(ec, key1.clone(), Digest::Sha256).unwrap();
        }
        KeyGenInfo::NistEc(ec::NistCurve::P384) => {
            info!("test_ec: {} signing test", kgi_to_str(info));
            let _ = test_ec_sign(ec, key1.clone(), Digest::Sha384).unwrap();
        }
        KeyGenInfo::NistEc(ec::NistCurve::P521) => {
            info!("test_ec: {} signing test", kgi_to_str(info));
            let _ = test_ec_sign(ec, key1.clone(), Digest::Sha512).unwrap();
        }
        KeyGenInfo::Ed25519 => {
            info!("test_ec: {} signing test", kgi_to_str(info));
            let _ = test_ec_sign(ec, key1.clone(), Digest::None).unwrap();
        }
        _ => {}
    }
}

/// Test [`Ec`] functionality.
pub fn test_ec<E: Ec>(ec: E) {
    test_ec_alg(&ec, &KeyGenInfo::Ed25519);
    test_ec_ed25519(&ec);
    test_ec_alg(&ec, &KeyGenInfo::X25519);
    test_ec_x25519(&ec);
    test_ec_alg(&ec, &KeyGenInfo::NistEc(ec::NistCurve::P224));
    test_ec_alg(&ec, &KeyGenInfo::NistEc(ec::NistCurve::P256));
    test_ec_alg(&ec, &KeyGenInfo::NistEc(ec::NistCurve::P384));
    test_ec_alg(&ec, &KeyGenInfo::NistEc(ec::NistCurve::P521));

    info!("test_ec: Success");
}

pub fn run() {
    info!("crypto::tests::run: Starting");

    test_derive_hbk();

    test_clock(crate::crypto::clock::Clock);

    test_eq(crate::crypto::eq::Eq);

    let mut rng = crate::crypto::rng::Rng;
    test_rng(&mut rng);

    test_sha256(crate::crypto::sha::Sha256 {});

    test_hmac(crate::crypto::mac::Hmac {});

    test_aes_cmac_128(crate::crypto::mac::AesCmac {});
    test_aes_cmac_192(crate::crypto::mac::AesCmac {});
    test_aes_cmac_256(crate::crypto::mac::AesCmac {});

    test_aes(crate::crypto::aes::Aes {});

    test_aes_gcm(crate::crypto::aes::Aes {});

    test_des(crate::crypto::des::Des {});

    test_rsa(crate::crypto::rsa::Rsa {});

    test_ec(crate::crypto::ec::Ec {});

    info!("crypto::tests::run: Done");
}
