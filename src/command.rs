//
// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Keymint TA command definitions.

// param[0]: input (unused)
// param[1]: output
pub const GET_AUTH_TOKEN_KEY: u32 = 512;

// param[0]: input
#[allow(dead_code)]
pub const WRITE: u32 = 1024;

// param[0]: output
#[allow(dead_code)]
pub const READ: u32 = 1025;

#[cfg(feature = "dev")]
pub const RUN_TEST: u32 = 2048;

#[cfg(feature = "dev")]
#[allow(dead_code)]
pub const KILL: u32 = 2049;
