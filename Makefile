#
# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

UUID := dc274baf-5f8c-48cd-89ab-cd9a4d38ed12
KEY ?= $(TA_DEV_KIT_DIR)/keys/default_ta.pem

TARGET ?= aarch64-unknown-linux-gnu
CROSS_COMPILE ?= aarch64-linux-gnu-

OBJCOPY := $(CROSS_COMPILE)objcopy
SIGN := $(TA_DEV_KIT_DIR)/scripts/sign_encrypt.py

OUTDIR := $(CURDIR)/target/$(TARGET)/release

all: ta strip sign

ta:
	@cargo build --target $(TARGET) --release --features dev

strip: ta
	@$(OBJCOPY) --strip-unneeded $(OUTDIR)/ta $(OUTDIR)/stripped_ta

sign: strip
	@$(SIGN) --uuid $(UUID) --key $(KEY) --in $(OUTDIR)/stripped_ta --out $(OUTDIR)/$(UUID).ta

clean:
	@cargo clean
