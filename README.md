# KeyMint reference implementation for OP-TEE

This repository holds a reference Rust KeyMint implementation for OP-TEE using the [Apache Teaclave TrustZone SDK](https://github.com/apache/incubator-teaclave-trustzone-sdk).
